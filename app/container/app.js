/**
 * Created by laith
 */
import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Navigator,
    Text,
    View,
    TouchableHighlight
} from 'react-native';

let LotsOfGreetings = require('../components/LotsOfGreetings');

let PizzaTranslator = require('../components/PizzaTranslator');

const styles = StyleSheet.create({
  boxDime: {
    width: 250,
    height: 50,
    left:50
  }
});

const routes = [
    {title: 'Home', index: 0},
    {title: 'PizzaTranslator', index: 1},
  ];


class AwesomeProjectApp extends Component {
    render() {
      return (
        <Navigator
            initialRoute = {{ title: 'Home', index: 0 }}
            style = {{ flex:1, paddingTop: 25}}
            renderScene = { this.renderScene }
        />
      );
    }

    renderScene(route, navigator) {
      if(route.index === 0) {
        return (
          <View style={[{flex: 1, backgroundColor: 'steelblue'}, styles.boxDime]}>
            <Text>welcome to my app</Text>
            <TouchableHighlight onPress = { () => {
              navigator.push({
                index: route.index + 1
              });
            }}>
              <Text>Tap me to load the next scene</Text>
            </TouchableHighlight>
          </View>
        )
      } else if ( route.index === 1 ) {
          return (
            <View>
              <PizzaTranslator />
              <TouchableHighlight onPress = { () => {
                navigator.push({
                  index: route.index + 1
                });
              }}>
                <Text>Tap me to load the next scene</Text>
              </TouchableHighlight>
              <TouchableHighlight onPress = { () => { navigator.pop(); }}>
                <Text>Tap me to go back</Text>
              </TouchableHighlight>
            </View>
          )
      } else if ( route.index === 2 ) {
          return (
            <View>
              <LotsOfGreetings name='Airen' last='Adamonis' />
              <TouchableHighlight onPress = { () => { navigator.pop(); }}>
                <Text>Tap me to go back</Text>
              </TouchableHighlight>
            </View>
        )
      }
    }
}

module.exports = AwesomeProjectApp;
export default AwesomeProjectApp;

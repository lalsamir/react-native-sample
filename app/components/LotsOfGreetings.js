
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text
} from 'react-native';

const styles = StyleSheet.create({
  bigtext: {
    fontWeight: 'bold',
    fontSize: 30
  },
  blue: {
    color: 'blue'
  }
});

class LotsOfGreetings extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    let display = `Hello ${this.props.name} ${this.props.last}! Today is ${this._getTodaysDay()}`;
    return (
      <Text style={[styles.bigtext, styles.blue]}>{display}</Text>
    );
  }

  _getTodaysDay() {
    let date = new Date();
    let weekday = new Array(7);
    weekday[0]=  "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
    return weekday[date.getDay()];
  }
}

module.exports = LotsOfGreetings;
export default LotsOfGreetings;
